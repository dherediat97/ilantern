package com.ilantern.dherediat.ilantern;
import android.hardware.Camera;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CompoundButton;
import android.widget.Switch;


public class ActividadPrincipal extends AppCompatActivity {

    private Camera mCamera = null;
    private Camera.Parameters parameters = null;


    //Si el movil no tiene flash que se encienda la pantalla en blanco
    //Si entra en el modo de ahorro de energia que siga funcionando
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

    }


    @Override
    protected void onResume() {
        super.onResume();
        linterna();
    }
    public void linterna(){
        final Switch button = (Switch) findViewById(R.id.linterna);
        mCamera = Camera.open();
        button.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton toggleButton, boolean on) {
                if (on) {
                    parameters = mCamera.getParameters();
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    mCamera.setParameters(parameters);
                    mCamera.startPreview();
                    button.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                } else if(!on) {
                    parameters = mCamera.getParameters();
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    mCamera.setParameters(parameters);
                    mCamera.stopPreview();
                    button.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                }
            }
        });

    }
}